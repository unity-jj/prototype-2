﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    public GameObject[] ballPrefabs;

    private float spawnLimitXLeft = -22;
    private float spawnLimitXRight = 7;
    private float spawnPosY = 15;

    private float startDelay = 1.0f;
    private float spawnIntervalStart = 3;
    private float spawnIntervalEnd = 5;

    // Start is called before the first frame update
    void Start()
    {
        float spawnInterval = Random.Range(spawnIntervalStart, spawnIntervalEnd);
        InvokeRepeating("SpawnRandomBall", startDelay, spawnInterval);
    }

    // Spawn random ball at random x position at top of play area
    void SpawnRandomBall ()
    {
        // Generate random ball index and random spawn position
        float randomX = Random.Range(spawnLimitXLeft, spawnLimitXRight);
        Vector3 spawnPos = new Vector3(randomX, spawnPosY, 0);
        int ballIndex = Random.Range(0, ballPrefabs.Length);
        GameObject ballToSpawn = ballPrefabs[ballIndex];

        // instantiate ball at random spawn location
        Instantiate(ballToSpawn, spawnPos, ballToSpawn.transform.rotation);
    }

}
