﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;
    private float timeDelay = 1.0f;

    // Update is called once per frame
    void Update()
    {
        timeDelay -= Time.deltaTime;

        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space) && timeDelay <= 0)
        {
            SendDog();
            timeDelay = 1.0f;
        }
    }

    void SendDog() {
        Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
    }
}
