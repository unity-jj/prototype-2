using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
  public GameObject[] animalPrefabs;
  private float spawnRangeX = 20;
  private float spawnMaxZ = 14;
  private float spawnPosZ = 20;
  private float startDelay = 2;
  private float spawnInterval = 1.5f;
  private float leftBound = -25;
  private float rightBound = 25;

  // Start is called before the first frame update
  void Start()
  {
    InvokeRepeating("SpawnRandomAnimal", startDelay, spawnInterval);
    InvokeRepeating("SpawnRandomAttackingAnimal", startDelay, spawnInterval);
  }

  // Update is called once per frame
  void Update()
  {
  }

  void SpawnRandomAnimal()
  {
    GameObject animalToSpawn = GetRandomAnimal();
    float randomX = Random.Range(-spawnRangeX, spawnRangeX);
    Vector3 spawnPos = new Vector3(randomX, 0, spawnPosZ);

    Instantiate(animalToSpawn, spawnPos, animalToSpawn.transform.rotation);
  }

  void SpawnRandomAttackingAnimal()
  {
    GameObject animalToSpawn = GetRandomAnimal();
    float randomZ = Random.Range(0, spawnMaxZ);
    int index = Random.Range(0, 2);
    float[] rotations = new float[2] { 270f, 90f };
    float[] edges = new float[2] { leftBound, rightBound };

    Vector3 spawnPos = new Vector3(edges[index], 0, randomZ);

    Instantiate(animalToSpawn, spawnPos, animalToSpawn.transform.rotation * Quaternion.Euler(0, rotations[index], 0));
  }

  private GameObject GetRandomAnimal()
  {
    int animalIndex = Random.Range(0, animalPrefabs.Length);
    return animalPrefabs[animalIndex];
  }
}
