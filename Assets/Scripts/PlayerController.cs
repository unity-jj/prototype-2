using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject projectilePrefab;
    private float horizontalInput;
    private float verticalInput;
    private float speed = 15.0f;
    private float zMin = 0;
    private float zMax = 15;
    private float xRange = 15;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // keepPlayerInBounds();
        movePlayer();

        if (Input.GetKeyDown(KeyCode.Space)) {
            shootProjectile();
        }
    }

    void LateUpdate() {
        keepPlayerInBounds();
    }

    void keepPlayerInBounds() {
        if (transform.position.x < -xRange) {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        } else if (transform.position.x > xRange) {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }

        if (transform.position.z <= zMin) {
            transform.position = new Vector3(transform.position.x, transform.position.y, zMin);
        } else if (transform.position.z > zMax) {
            transform.position = new Vector3(transform.position.x, transform.position.y, zMax);
        }
    }

    void movePlayer() {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.right * horizontalInput * speed * Time.deltaTime);
        transform.Translate(Vector3.forward * verticalInput * speed * Time.deltaTime);
    }

    void shootProjectile() {
        Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
    }
}
